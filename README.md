## Labour Delivery ( Laravel 10 )

**Requirements**

1. PHP 8.1 - 8.2
2. composer

**Setup**

 - `git clone https://gitlab.com/itxjashan/labour-delivery.git`

 - `cd labour-delivery && composer install`
 -  `cp .env.example .env`
 - Set following configurations in .env:	
		 

    - DB_*
    - MAIL_*
    - STRIPE_KEY
    - STRIPE_SECRET

 
 5. `php artisan migrate --seed`

**Assets**

 1. [Workflow](https://docs.google.com/document/d/12CUqNuDWR53rSB6S9rMrfgaFSHrklSzA8d5vRAuG00k/edit)
 2. [Invision](https://projects.invisionapp.com/share/JV138OE6U6ES#/screens/473199390)

**Notes**

 1. [Please follow these naming conventions](https://xqsit.github.io/laravel-coding-guidelines/docs/naming-conventions/) - You can also follow other standards listed in the link.
