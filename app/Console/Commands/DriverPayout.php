<?php

namespace App\Console\Commands;

use App\Models\Delivery;
use App\Models\DriverTip;
use Illuminate\Console\Command;
use App\Models\DriverPayout as ModelDriverPayout;
use Exception;
use Carbon\Carbon;

class DriverPayout extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'driver:payout';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Driver fund transfer';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        // delivery..
        self::deliveryAmountTransferFromDriver();

        // driver tip..
        self::tipAmountTransferFromDriver();
    }

    private function deliveryAmountTransferFromDriver()
    {

        $deliveries = Delivery::with('assignDriver', 'payment')
                        ->where(function($quesry){
                            $quesry->where('status', 'delivered_by_driver');
                            $quesry->orWhere('status', 'accepted_by_customer');
                        })
                        ->where('updated_at', '<', Carbon::parse('-3 hours'))
                        ->where('is_payout_driver', '0')
                        ->get();

        foreach($deliveries as $delivery){

            if(!empty($delivery->assignDriver->user->bankInfo)){

                $bankInfo = $delivery->assignDriver->user->bankInfo;

                if($bankInfo->payouts_enabled == 'active'){

                    try{
                        $totalAmount = $delivery->payment->amount;
                        $connectedAccountId = $bankInfo->account_id;
                        $driverAmount = ($totalAmount) ? ($totalAmount/100)*40 : 0;

                        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

                        $transfer = \Stripe\Transfer::create([
                            'amount' => round($driverAmount * 100),
                            'currency' => 'usd',
                            'destination' => $connectedAccountId,
                        ]);

                        $delivery->update(['is_payout_driver' => '1']);

                        $driverPay = new ModelDriverPayout;
                        $driverPay->users_id = $delivery->assignDriver->user->id;
                        $driverPay->delivery_id = $delivery->id;
                        $driverPay->amount = $driverAmount;
                        $driverPay->transaction_id = isset($transfer['id']) ? $transfer['id'] : null;
                        $driverPay->type = 'delivery';
                        $driverPay->save();

                    }catch (
                        \Stripe\Exception\RateLimitException |
                        \Stripe\Exception\InvalidRequestException |
                        \Stripe\Exception\AuthenticationException |
                        \Stripe\Exception\ApiConnectionException |
                        \Stripe\Exception\ApiErrorException |
                        Exception $e
                    ) {
                        $error = $e->getMessage();
                        if($error){
                            // var_dump($error);
                        }
                    }
                }
            }
        }
    }

    private function tipAmountTransferFromDriver()
    {
        $driverTips = DriverTip::with('driver')->where('is_payout_driver', '0')->get();

        foreach($driverTips as $driverTip){
            if(!empty($driverTip->driver->bankInfo)){
                $bankInfo = $driverTip->driver->bankInfo;
                if($bankInfo->payouts_enabled == 'active'){
                    try{

                        $totalAmount = $driverTip->amount;
                        $connectedAccountId = $bankInfo->account_id;

                        // $driverAmount = ($totalAmount) ? ($totalAmount/100)*40 : 0;

                        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

                        $transfer = \Stripe\Transfer::create([
                            'amount' => round($totalAmount * 100),
                            'currency' => 'usd',
                            'destination' => $connectedAccountId,
                        ]);

                        $driverPay = new ModelDriverPayout;
                        $driverPay->users_id = $driverTip->to_user_id;
                        $driverPay->delivery_id = $driverTip->delivery_id;
                        $driverPay->amount = $totalAmount;
                        $driverPay->transaction_id = isset($transfer['id']) ? $transfer['id'] : null;
                        $driverPay->type = 'tip';
                        $driverPay->save();

                        $driverTip->update(['is_payout_driver' => '1']);

                    }catch (
                        \Stripe\Exception\RateLimitException |
                        \Stripe\Exception\InvalidRequestException |
                        \Stripe\Exception\AuthenticationException |
                        \Stripe\Exception\ApiConnectionException |
                        \Stripe\Exception\ApiErrorException |
                        Exception $e
                    ) {
                        $error = $e->getMessage();
                        if($error){
                            // var_dump($error);
                        }
                    }
                }
            }
        }
    }
}
