<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use App\Enums\UserRole;
use App\Models\Customer;
use App\Models\Delivery;
use App\Models\Store;

class AdminController extends Controller
{
    public function dashboard()
    {
        $deliveriesCount = Delivery::count();
        $usersCount = Customer::count();
        $driversCount = User::where('role', UserRole::Driver)->count();
        $storesCount = Store::count();
        return view('admin.dashboard', compact('deliveriesCount', 'usersCount', 'driversCount', 'storesCount'));
    }

    public function customer()
    {
        $title = [
            'title'=> 'Customers',
            'active'=> 'customer',
        ];

        $customers = Customer::latest()->get();
        return view('admin.user.customer-list', compact('customers', 'title'));
    }

    public function driver()
    {
        $title = [
            'title'=> 'Driver',
            'active'=> 'driver',
        ];

        $drivers = User::where('role', UserRole::Driver)->latest()->get();

        return view('admin.user.driver-list', compact('title', 'drivers'));
    }

    public function delivery()
    {
        $title = [
            'title'=> 'Delivery',
            'active'=> 'delivery',
        ];

        $deliveries = Delivery::with('store', 'user', 'assignDriver.user')->latest()->get();
      
        return view('admin.delivery.delivery-list', compact('deliveries', 'title'));
    }
}
