<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Store;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class StoreController extends Controller
{
    //
    use LivewireAlert;

    // list..
    public function index()
    {
        $title = [
            'title'=>'Stores',
            'active'=> 'store',
        ];
        $stores = Store::latest()->get();

        return view('admin.store.index', compact('stores', 'title'));
    }

    // create..
    public function create()
    {
        $title = [
            'title'=>'store',
            'active'=> 'store',
        ];
        return view('admin.store.create', compact('title'));
    }

    // store..
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'url' => 'required',
            'file' => 'required',
        ]);

        $store = new Store;
        $store->title = $request->title;
        $store->url = $request->url;
        $store->save();

        if($request->hasFile('file') && $request->file('file')->isValid()){
            $store->addMediaFromRequest('file')->toMediaCollection('logo');
        }

        $this->flash('success', 'Store has been created');
        return redirect()->route('admin.store.index');
    }

    // edit..
    public function edit($id)
    {
        $title = [
            'title'=>'store',
            'active'=> 'store',
        ];
        $store = Store::findOrFail($id);
        return view('admin.store.edit', compact('store', 'title'));
    }

    // update..
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'url' => 'required',
        ]);

        $store = Store::find($id);
        $store->title = $request->title;
        $store->url = $request->url;
        $store->save();

        if($request->hasFile('file')){
            $store->clearMediaCollection('logo');
            $store->addMediaFromRequest('file')->toMediaCollection('logo');
        }
        $this->flash('success', 'Store has been updated');
        return redirect()->route('admin.store.index');
    }

    //delete..
    public function destroy($id)
    {
        $store = Store::findOrFail($id);
        $store->delete();

        $this->flash('success', 'Store has been deleted');
        return redirect()->route('admin.store.index');
    }
}
