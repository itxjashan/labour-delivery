<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{

    public function signUpCustomer()
    {
        $title = 'signup-customer';
        return view('auth.register-customer', compact('title'));
    }

    public function signUpDriver()
    {
        $title = 'signup-driver';
        return view('auth.register-driver', compact('title'));
    }
}
