<?php

namespace App\Http\Controllers\Customer;

use App\Enums\DeliveryStatus;
use App\Http\Controllers\Controller;
use App\Models\Delivery;
use Illuminate\Http\Request;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class CustomerController extends Controller
{
    //
    use LivewireAlert;

    public function dashboard()
    {
        $title = "select store";

        return view('customer.schedule-delivery', compact('title'));
    }

    public function deliveryList()
    {
        $deliveries = Delivery::with('store')->where('users_id', auth()->user()->id)->latest()->get();
      
        return view('customer.delivery-list', compact('deliveries'));
    }

    public function viewDelivery($id)
    {
        $delivery = Delivery::with('store')->find(decrypt($id));
       
        return view('customer.view-delivery', compact('delivery'));
    }

}
