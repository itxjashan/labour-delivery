<?php

namespace App\Http\Controllers\Driver;

use App\Http\Controllers\Controller;
use App\Models\Delivery;
use Illuminate\Http\Request;
use App\Models\DeliveryAssignedDriver;
use App\Enums\DeliveryStatus;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class DriverController extends Controller
{
    //
    use LivewireAlert;

    public function dashboard()
    {
        return view('driver.delivery-board');
    }

    public function approvedDelivery()
    {
        return view('driver.approved-delivery');
    }

}
