<?php

namespace App\Http\Livewire\Customer;

use Livewire\Component;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use App\Models\Delivery as DeliveryModel;
use App\Models\Store;
use App\Models\UserCard;
use \Stripe\Stripe;
use \Stripe\StripeClient;
use \Stripe\Customer;
use App\Enums\DeliveryStatus;
use App\Models\Payment;
use Livewire\WithFileUploads;
use Exception;


class Delivery extends Component
{
    use LivewireAlert;
    use WithFileUploads;

    // Step configuration
    public $totalSteps = 7;
    public $currentActiveStep = 1;

    // User details
    public $user;

    // Select store
    public $store_id, $stores, $store, $cart_error, $e_reciept;

    // Receipt fields
    public $store_name, $store_address, $pickup_date, $pickup_time, $customError;

    // Schedule delivery
    public $delivery_date, $delivery_time, $delivery_address, $phone_number, $customer_name, $pickup_need, $additional_note;

    // Card details
    public $card, $cardCheck = false, $card_status, $card_name, $card_number, $exp_month, $exp_year, $cvv;


    public function mount()
    {
        // Initialize component data
        $this->currentActiveStep = 1;
        $this->stores = Store::get();
        $this->user = auth()->user();
        $this->card = UserCard::where('users_id', auth()->user()->id)->latest()->first();

        if ($this->card) {
            $this->cardCheck = false;
        } else {
            $this->cardCheck = true;
        }
    }

    // Check card selection
    public function updatedCardStatus($value)
    {
        if ($value == 'add_new_card') {
            $this->resetErrorBag();
            $this->cardCheck = true;
        } else {
            $this->resetErrorBag();
            $this->cardCheck = false;
        }
    }

    // Check file type
    public function validateFileType()
    {
        if ($this->e_reciept) {
            $allowedFileTypes = ['image/jpeg', 'image/png', 'image/gif', 'application/pdf'];

            $fileMimeType = $this->e_reciept->getMimeType();

            if (!in_array($fileMimeType, $allowedFileTypes)) {
                $this->reset('e_reciept');
                $this->addError('e_reciept', 'Only images (JPEG, PNG, GIF) or PDF files are allowed.');
            }
        }
    }

    // Reset form fields
    public function resetFields()
    {
        $this->store_id = '';
        $this->cart_error = '';
        $this->store_name = '';
        $this->store_address = '';
        $this->pickup_date = '';
        $this->pickup_time = '';
        $this->delivery_date = '';
        $this->delivery_time = '';
        $this->delivery_address = '';
        $this->phone_number = '';
        $this->customer_name = '';
        $this->pickup_need = '';
        $this->additional_note = '';
        $this->card_name = '';
        $this->card_number = '';
        $this->exp_month = '';
        $this->exp_year = '';
        $this->cvv = '';
    }

    // Proceed to the next step
    public function increaseStep()
    {
        $this->validateData();

        if ($this->customError) {
            $this->currentActiveStep = $this->currentActiveStep;
            return back();
        }

        $this->currentActiveStep += 1;

        if ($this->currentActiveStep > $this->totalSteps) {
            $this->currentActiveStep = $this->totalSteps;
        }

        $this->emit('selectFire', $this->pickup_date, $this->pickup_time);
        $this->emit('selectFire2', $this->delivery_date, $this->delivery_time);
    }

    // Go back to the previous step
    public function decreaseStep()
    {
        $this->customError = '';
        $this->currentActiveStep -= 1;
        $this->emit('selectFire', $this->pickup_date, $this->pickup_time);
        $this->emit('selectFire2', $this->delivery_date, $this->delivery_time);
    }

    // Validation form
    public function validateData()
    {
        if ($this->currentActiveStep == 1) {
            $this->validate(
                [
                    'store_id' => 'required',
                ],
                [
                    'store_id.required' => 'The :attribute cannot be empty.',
                ]
            );
        } elseif ($this->currentActiveStep == 4) {
            $this->validate(
                [
                    'store_name' => 'required',
                    'store_address' => 'required',
                    'pickup_date' => 'required',
                    'pickup_time' => 'required'
                ]
            );
        } elseif ($this->currentActiveStep == 5) {
            // Check if delivery time is greater than pickup time
            if ($this->pickup_date == $this->delivery_date) {
                $fromTime = strtotime($this->pickup_time);
                $toTime = strtotime($this->delivery_time);
                if ($fromTime >= $toTime) {
                    $this->customError = 'Delivery time must be greater than pickup time ' . $this->pickup_time;
                } else {
                    $this->customError = '';
                }
            }

            // Validate delivery form data
            $this->validate([
                'delivery_date' => 'required',
                'delivery_time' => 'required',
                'delivery_address' => 'required',
                'phone_number' => 'required|min:12',
                'customer_name' => 'required',
                'pickup_need' => 'required',
                'delivery_date' => 'required',
            ], [
                'phone_number.min' => 'The phone number must be 10 digits.'
            ]);
        } elseif ($this->currentActiveStep == 6) {
            // Check if delivery time is greater than pickup time
            if ($this->pickup_date == $this->delivery_date) {
                $fromTime = strtotime($this->pickup_time);
                $toTime = strtotime($this->delivery_time);
                if ($fromTime >= $toTime) {
                    $this->customError = 'Delivery time must be greater than pickup time ' . $this->pickup_time;
                } else {
                    $this->customError = '';
                }
            }

            // Validate delivery form data
            $this->validate([
                'store_name' => 'required',
                'store_address' => 'required',
                'pickup_date' => 'required',
                'pickup_time' => 'required',
                'delivery_date' => 'required',
                'delivery_time' => 'required',
                'delivery_address' => 'required',
                'phone_number' => 'required|min:12',
                'customer_name' => 'required',
                'pickup_need' => 'required',
                'delivery_date' => 'required',
            ], [
                'phone_number.min' => 'The phone number must be 10 digits.'
            ]);
        } elseif ($this->currentActiveStep == 7) {
            // Validate card details

            if ($this->cardCheck) {
                $this->validate([
                    'card_name' => 'required',
                    'card_number' => 'required',
                    'exp_month' => 'required',
                    'exp_year' => 'required',
                    'cvv' => 'required|numeric|digits_between:3,4',
                ]);
            } else {
                $this->validate(
                    [
                        'card_status' => 'required',
                        'card_name' => 'required_if:card_status,add_new_card',
                        'card_number' => 'required_if:card_status,add_new_card',
                        'exp_month' => 'required_if:card_status,add_new_card',
                        'exp_year' => 'required_if:card_status,add_new_card',
                        'cvv' => 'required_if:card_status,add_new_card|' . ($this->card_status == 'add_new_card' ? 'numeric' : '') . '|' . ($this->card_status == 'add_new_card' ? 'digits_between:3,4' : ''),
                    ],
                    [
                        'card_status.required' => 'Please select the card option.'
                    ]
                );
            }
        }
    }

    // Update selected store details
    public function updatedStoreId()
    {
        if ($this->store_id) {
            $this->store = Store::findOrFail($this->store_id);
            $this->store_name = $this->store->title;
        }
    }

    // Store delivery details
    public function store()
    {
        $this->validateData();

        $this->submitForm();
    }

    // Store delivery details
    public function submitForm()
    {
        try {

            \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
            $stripe = new \Stripe\StripeClient(config('services.stripe.secret'));

            if ($this->cardCheck) {
                $token = $this->createStripeToken($stripe); // Create Stripe token
                $customer = $this->createCustomer($stripe, $token); // Create customer in Stripe
                $charge = $this->createCharge($stripe, $customer['id']); // Create charge in Stripe
                $card = $this->saveUserCard($customer['id'], $token); // Save user card details

            } else {
                $customerId = $this->card->customer_id;
                $card = $this->card;
                $charge = $this->createCharge($stripe, $customerId); // Create charge in Stripe
            }


            $delivery = $this->createDelivery(); // Create delivery
            $payment = $this->createPayment($delivery->id, $card->id, $charge); // Create payment

            $this->resetFields(); // Reset form fields
            $this->alert('success', 'Delivery added successfully'); // Show success alert

            return redirect()->route('customer.delivery_list'); // Redirect to delivery list page
        } catch (
            \Stripe\Exception\RateLimitException |
            \Stripe\Exception\InvalidRequestException |
            \Stripe\Exception\AuthenticationException |
            \Stripe\Exception\ApiConnectionException |
            \Stripe\Exception\ApiErrorException |
            Exception $e
        ) {
            $error = $e->getMessage();
        }

        if (isset($error)) {
            $this->cart_error = $error;
        }
    }

    private function createStripeToken($stripe)
    {
        $token = $stripe->tokens->create([
            'card' => [
                'name' => $this->card_name,
                'number' => str_replace(' ', '', $this->card_number),
                'exp_month' => $this->exp_month,
                'exp_year' => $this->exp_year,
                'cvc' => $this->cvv,
            ],
        ]);

        return $token;
    }

    private function createCustomer($stripe, $token)
    {
        $customer = $stripe->customers->create([
            'source' => $token['id'],
            'email' => $this->user->email,
            'description' => 'My name is ',
        ]);

        return $customer;
    }

    private function createCharge($stripe, $customerId)
    {
        $amount = '25';

        $charge = $stripe->charges->create([
            "amount" => $amount * 100,
            "currency" => "usd",
            "customer" => $customerId,
            "description" => "Delivery",
            "capture" => true,
        ]);

        return $charge;
    }

    private function saveUserCard($customerId, $token)
    {

        $card = new UserCard();

        // Save user card details

        $card->users_id = $this->user->id;
        $card->customer_id = $customerId;
        $card->card_id = $token->card->id;
        $card->card_name = $this->card_name;
        $card->card_number = $token->card->last4;
        $card->exp_month = $token->card->exp_month;
        $card->exp_year = $token->card->exp_year;
        $card->save();

        return $card;
    }

    private function createDelivery()
    {
        $delivery = new DeliveryModel();

        // Set delivery details
        $delivery->users_id = auth()->user()->id;
        $delivery->stores_id = $this->store_id;
        $delivery->store_name = $this->store_name;
        $delivery->store_address = $this->store_address;
        $delivery->pickup_date = $this->pickup_date;
        $delivery->pickup_time = $this->pickup_time;
        $delivery->delivery_date = $this->delivery_date;
        $delivery->delivery_time = $this->delivery_time;
        $delivery->delivery_address = $this->delivery_address;
        $delivery->phone_number = $this->phone_number;
        $delivery->customer_name = $this->customer_name;
        $delivery->pickup_need = $this->pickup_need;
        $delivery->additinal_note = $this->additional_note;
        $delivery->status = DeliveryStatus::Pending;
        $delivery->save();

        return $delivery;
    }

    private function createPayment($deliveryId, $cardId, $charge)
    {
        $amount = '25';
        $payment = new Payment();

        // Set payment details
        $payment->users_id = $this->user->id;
        $payment->delivery_id = $deliveryId;
        $payment->user_cards_id = $cardId;
        $payment->transaction_id = $charge->id;
        $payment->balance_transaction = $charge->balance_transaction;
        $payment->customer = $charge->customer;
        $payment->currency = $charge->currency;
        $payment->amount = $amount;
        $payment->payment_status = $charge->status;
        $payment->save();

        return $payment;
    }

    public function render()
    {
        return view('livewire.customer.delivery');
    }
}
