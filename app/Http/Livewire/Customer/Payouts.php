<?php

namespace App\Http\Livewire\Customer;

use Livewire\Component;
use App\Models\DriverPayout;
use Livewire\WithPagination;
use DB;
use App\Models\Delivery;
use App\Models\DriverTip;
use App\Models\Payment;

class Payouts extends Component
{
    use WithPagination;

    public $search;
    public $amount = [];

    protected $paginationTheme = 'bootstrap';

    public function showModal($id)
    {
        $this->amount['delivery'] = Payment::where('delivery_id', $id)->sum('amount');
        $this->amount['tip'] = DriverTip::where('delivery_id', $id)->sum('amount');

        $this->emit('checkAmount');
    }
    public function render()
    {
        $deliveries = Delivery::with('store')
                    ->withSum('tip', 'amount')
                    ->withSum('payment', 'amount')
                    ->where('users_id', auth()->user()->id)
                    ->whereHas('payment')

                    ->where(function ($query) {
                        $searchTerm = $this->search;
                        if ($searchTerm) {
                            $query->where('delivery_address', 'like', '%' . $searchTerm . '%');
                            $query->orWhere('delivery_date', 'like', '%' . date('m/d/Y', strtotime($searchTerm)) . '%');
                            $query->orWhere('delivery_time', 'like', '%' . $searchTerm . '%');
                            $query->orWhereHas('store', function ($subQuery) use ($searchTerm) {
                                $subQuery->where('title', 'like', '%' . $searchTerm . '%');
                            });
                        }
                    })
                    ->latest()
                    ->paginate(5);
                    // dd($deliveries);
        return view('livewire.customer.payouts', compact('deliveries'))->extends('layouts.app');
    }
}
