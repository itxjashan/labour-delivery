<?php

namespace App\Http\Livewire\Driver;

use App\Models\DeliveryAssignedDriver;
use App\Models\Delivery;
use App\Models\Store;
use Livewire\Component;
use Livewire\WithPagination;

class ApprovedDelivery extends Component
{
    use WithPagination;

    public $search;

    protected $paginationTheme = 'bootstrap';

    public function render()
    {

        $deliveries = DeliveryAssignedDriver::with('delivery.store')->where('driver_id', auth()->user()->id)
                        ->where(function($query) {
                            $searchTerm = $this->search;
                            if ($searchTerm) {
                                $query->whereHas('delivery.store', function ($subQuery) use ($searchTerm) {
                                    $subQuery->where('delivery_address', 'like', '%' . $searchTerm . '%');
                                    $subQuery->orWhere('delivery_date', 'like', '%' . $searchTerm . '%');
                                    $subQuery->orWhere('delivery_time', 'like', '%' . $searchTerm . '%');
                                    $subQuery->orWhere('title', 'like', '%' . $searchTerm . '%');
                                });
                            }
                        })
                        ->latest()
                        ->paginate(5);
        return view('livewire.driver.approved-delivery', compact('deliveries'));
    }
}
