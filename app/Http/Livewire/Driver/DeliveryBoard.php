<?php

namespace App\Http\Livewire\Driver;

use Livewire\Component;
use App\Models\Delivery;
use Livewire\WithPagination;

class DeliveryBoard extends Component
{
    use WithPagination;
    public $search;

    protected $paginationTheme = 'bootstrap';

    public function render()
    {
        $deliveries = Delivery::with('store')->where(function($quesry){
                            $quesry->where('status', 'pending');
                            $quesry->orWhere('status', 'cancelled');
                        })
                        ->where(function($query) {
                            $searchTerm = $this->search;
                            if ($searchTerm) {
                                $query->where('delivery_address', 'like', '%' . $searchTerm . '%');
                                $query->orWhere('delivery_date', 'like', '%' . $searchTerm . '%');
                                $query->orWhere('delivery_time', 'like', '%' . $searchTerm . '%');
                                $query->orWhereHas('store', function ($subQuery) use ($searchTerm) {
                                    $subQuery->where('title', 'like', '%' . $searchTerm . '%');
                                });
                            }
                        })
                        ->latest()
                        ->paginate(5);
        
        return view('livewire.driver.delivery-board', compact('deliveries'));
    }
}
