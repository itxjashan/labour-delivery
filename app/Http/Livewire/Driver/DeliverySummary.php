<?php

namespace App\Http\Livewire\Driver;

use Livewire\Component;
use App\Models\Delivery;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use App\Mail\DeliveryStatus as DeliveryStatusMail;
use Exception;
use Illuminate\Support\Facades\Mail;
use App\Enums\DeliveryStatus;
use App\Models\DeliveryAssignedDriver;

class DeliverySummary extends Component
{
    use LivewireAlert;

    //..
    public $delivery;

    // receipt fields
    public $store_name, $store_address, $pickup_date, $pickup_time;

    // schedule delivery
    public $deliveryId, $delivery_date, $delivery_time, $delivery_address, $phone_number, $customer_name, $pickup_need, $additinal_note;

    public function mount($id)
    {
        $this->delivery = Delivery::with('store')->find(decrypt($id));
    }

    public function deliveryAction($id, $action)
    {
        $delivery = Delivery::with('user')->find($id);
        $assignedDelivery = DeliveryAssignedDriver::where('driver_id', auth()->user()->id)->where('delivery_id', $id)->first();

        if($action == 'begin_route'){
            $delivery->update(['status'=> DeliveryStatus::Inprocess]);
            $assignedDelivery->update(['status'=> DeliveryStatus::Inprocess]);
            $message = 'Delivery in process';
        }elseif($action == 'delivered'){
            $delivery->update(['status'=> DeliveryStatus::DeliveredByDriver]);
            $assignedDelivery->update(['status'=> DeliveryStatus::DeliveredByDriver]);
            $message = 'Delivery completed';
        }else{
            $delivery->update(['status'=> DeliveryStatus::Cancelled]);
            $assignedDelivery->update(['status'=> DeliveryStatus::Cancelled]);
            $message = 'Delivery cancelled';
        }

        // email..
        if (isEmail()) {
            try {
                Mail::to($delivery->user->email)->send(new DeliveryStatusMail($delivery, $action));
            } catch (Exception $e) {
            }
        }

        $this->alert('success', $message);
        return redirect()->route('driver.approved_delivery');
    }

    public function render()
    {

        return view('livewire.driver.delivery-summary')->extends('layouts.app');
    }
}
