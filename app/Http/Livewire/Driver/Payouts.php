<?php

namespace App\Http\Livewire\Driver;

use App\Models\Delivery;
use Livewire\Component;
use App\Models\DriverPayout;
use Livewire\WithPagination;
use DB;

class Payouts extends Component
{
    use WithPagination;

    public $search;
    public $amount = [];

    protected $paginationTheme = 'bootstrap';

    public function showModal($id)
    {
        $this->amount['delivery'] = DriverPayout::where('delivery_id', $id)->where('type', 'delivery')->sum('amount');
        $this->amount['tip'] = DriverPayout::where('delivery_id', $id)->where('type', 'tip')->sum('amount');

        $this->emit('checkAmount');
    }

    public function render()
    {
        $payouts = DriverPayout::with('delivery')
            ->where('users_id', auth()->user()->id)

            ->where(function ($query) {
                $searchTerm = $this->search;
                if ($searchTerm) {
                    $query->whereHas('delivery.store', function ($subQuery) use ($searchTerm) {
                        $subQuery->where('delivery_address', 'like', '%' . $searchTerm . '%');
                        $subQuery->orWhere('delivery_date', 'like', '%' . date('m/d/Y', strtotime($searchTerm)) . '%');
                        $subQuery->orWhere('delivery_time', 'like', '%' . $searchTerm . '%');
                        $subQuery->orWhere('title', 'like', '%' . $searchTerm . '%');
                    });
                }
            })
            ->select('delivery_id', DB::raw('SUM(amount) as total_amount'))
            ->groupBy('delivery_id')
            ->latest()
            ->paginate(5);

            // dd($payouts);
        return view('livewire.driver.payouts', compact('payouts'))->extends('layouts.app');
    }
}
