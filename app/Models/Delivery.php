<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $appends = [
        'time', 'date', 'pick_date', 'pick_time'
    ];

    // appends function
    public function getPickDateAttribute()
    {
        $date = '';
        if ($this->pickup_date) {
            $date = date('m/d/Y', strtotime($this->pickup_date));
        }
        return $date;
    }

    public function getPickTimeAttribute()
    {
        $time = '';
        if ($this->pickup_time) {
            $time = date('h:i a', strtotime($this->pickup_time));
        }
        return $time;
    }

    public function getTimeAttribute()
    {
        $time = '';
        if ($this->delivery_time) {
            $time = date('h:i a', strtotime($this->delivery_time));
        }
        return $time;
    }

    public function getDateAttribute()
    {
        $date = '';
        if ($this->delivery_date) {
            $date = date('m/d/Y', strtotime($this->delivery_date));
        }
        return $date;
    }

    // Status Class
    public function statusClass()
    {
        $statuses = [
            'pending'   => 'question-img',
            'inprocess'   => 'question-img',
            'accepted'   => 'question-img',
            'delivered_by_driver'  => 'correct-img',
            'accepted_by_customer'  => 'correct-img',
            'cancelled' => 'cross-img',
        ];

        return $statuses[$this->status];
    }

    // Delivery Status
    public function deliveryStatusByCustomer()
    {
        $statuses = [
            'pending'   => 'Pending',
            'inprocess'   => 'Order Received',
            'accepted'   => 'Accepted',
            'delivered_by_driver'  => 'Reached Destination',
            'accepted_by_customer'  => 'Delivered',
            'cancelled' => 'Cancelled',
        ];

        return $statuses[$this->status];
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id', 'id');
    }

    public function store()
    {
        return $this->belongsTo(Store::class, 'stores_id', 'id');
    }

    public function assignDriver()
    {
        return $this->hasOne(DeliveryAssignedDriver::class, 'delivery_id', 'id')->latest();
    }

    public function payment()
    {
        return $this->hasOne(Payment::class, 'delivery_id', 'id');
    }

    public function payouts()
    {
        return $this->hasMany(DriverPayout::class, 'delivery_id', 'id');
    }

    public function tip()
    {
        return $this->hasOne(DriverTip::class, 'delivery_id', 'id');
    }
}
