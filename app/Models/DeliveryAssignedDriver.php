<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryAssignedDriver extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function delivery()
    {
        return $this->belongsTo(Delivery::class, 'delivery_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'driver_id', 'id');
    }

    // Status Class
    public function statusClass()
    {
        $statuses = [
            'pending'   => 'question-img',
            'inprocess'   => 'question-img',
            'accepted'   => 'question-img',
            'delivered_by_driver'  => 'correct-img',
            'accepted_by_customer'  => 'correct-img',
            'cancelled' => 'cross-img',
        ];

        return $statuses[$this->status];
    }

    // Delivery Status
    public function deliveryStatusByDriver()
    {
        $statuses = [
            'pending'   => 'Pending',
            'inprocess'   => 'Order Received',
            'accepted'   => 'Accepted',
            'delivered_by_driver'  => 'Delivered',
            'accepted_by_customer'  => 'Delivered',
            'cancelled' => 'Cancelled',
        ];

        return $statuses[$this->status];
    }
}
