<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DriverPayout extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function delivery()
    {
        return $this->belongsTo(Delivery::class, 'delivery_id', 'id');
    }

   public function deliveryTip()
   {
        return DriverPayout::where('delivery_id', $this->delivery_id)->where('type', 'tip')->sum('amount');
   }

   public function deliveryCarge()
   {
        return DriverPayout::where('delivery_id', $this->delivery_id)->where('type', 'delivery')->sum('amount');
   }


}
