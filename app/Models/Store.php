<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Store extends Model implements HasMedia
{
    use HasFactory;


    protected $fillable = [
        'title',
        'logo',
        'url',
        'email',
    ];

    use HasFactory, InteractsWithMedia;

    protected $guarded = ['id'];
}
