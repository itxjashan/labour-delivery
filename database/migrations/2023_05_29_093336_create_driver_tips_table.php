<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('driver_tips', function (Blueprint $table) {
            $table->id();
            $table->integer('from_user_id');
            $table->integer('to_user_id');
            $table->integer('delivery_id')->nullable();
            $table->string('transaction_id', 50)->nullable();
            $table->string('balance_transaction', 50)->nullable();
            $table->string('customer', 50)->nullable();
            $table->string('currency');
            $table->decimal('amount', 8, 2);
            $table->string('payment_status');
            $table->enum('is_payout_driver', ['0', '1'])->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('driver_tips');
    }
};
