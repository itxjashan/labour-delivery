<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bank_infos', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('account_id')->nullable();
            $table->string('status',50)->nullable();
            $table->string('payouts_enabled',20)->nullable();
            $table->string('account_number',30)->nullable();
            $table->string('routing_number',30)->nullable();
            $table->string('account_holder_name',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bank_infos');
    }
};
