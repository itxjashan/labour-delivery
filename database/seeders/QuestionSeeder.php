<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Question;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
                [
                    'title'=> 'Were your items delivered on time?'
                ],
                [
                    'title'=> 'Were any of your items damaged?'
                ]
            ];

        Question::insert($data);
    }
}
