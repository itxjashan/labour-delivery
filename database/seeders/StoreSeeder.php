<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Store;

class StoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $payloads = [
            [
                'title' => 'The Home Depot',
                'url' => 'https://www.homedepot.com/',
                'logo' => 'home-depot-logo.png',
            ],
            [
                'title' => 'Lowe’s',
                'url' => 'https://www.lowes.com/',
                'logo' => 'lowes logo 1.png',
            ],
            [
                'title' => 'McCoy’s Building Supply',
                'url' => 'https://www.mccoys.com/',
                'logo' => 'mccoys logo 1.png',
            ],
            [
                'title' => 'Ace Hardware',
                'url' => 'https://www.acehardware.com/',
                'logo' => 'store-location-logo 1.png',
            ]
        ];

        foreach($payloads as $payload){
            $store = new Store();
            $store->title = $payload['title'];
            $store->url = $payload['url'];
            $store->logo = $payload['logo'];
            $store->save();

            $store->addMedia(public_path('img/'.$payload['logo']))
            ->preservingOriginal()
            ->toMediaCollection('logo');
        }
    }
}
