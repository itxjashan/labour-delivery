<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\Customer;
use App\Enums\UserRole;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create([
            'first_name' => "ITX",
            'last_name' => "Admin",
            'email' => "admin@itx.com",
            'role' => UserRole::Admin,
            'password' => Hash::make('12345678'),
            'email_verified_at' => now(),
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $customer = Customer::create(['company_name' => 'ITX']);
        User::create([
            'first_name' => "ITX",
            'last_name' => "Customer",
            'email' => "customer@itx.com",
            'role' => UserRole::Customer,
            'userable_type' => Customer::class,
            'userable_id' => $customer->id,
            'password' => Hash::make('12345678'),
            'email_verified_at' => now(),
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        User::create([
            'first_name' => "ITX",
            'last_name' => "Driver",
            'email' => "driver@itx.com",
            'role' => UserRole::Driver,
            'password' => Hash::make('12345678'),
            'email_verified_at' => now(),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
