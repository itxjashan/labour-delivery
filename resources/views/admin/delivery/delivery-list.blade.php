@extends('layouts.admin-app')
@section ('title', 'Deliveries')
@section('content')
<div class="container p-5 ">

    <div class="row">
        <div class="col-md-6 mb-4 ">
            <div class="pull-right">
               <h2> Deliveries </h2>
            </div>
        </div>
    </div>

    <div class="container mt-2 custom-table">

        <table class="table table-bordered pt-4 " id="myTable">
            <thead>
                <tr>
                    <th>Sr. No</th>
                    <th>Store Name</th>
                    <th>Delivery Date</th>
                    <th>Delivery Time</th>
                    <th>Delivery Address</th>
                    <th>Phone Number</th>
                    <th>Assign Driver</th>
                    <th>Delivery Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($deliveries as $delivery)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td><b>{{ $delivery->store->title }}</b></td>
                        <td>{{ $delivery->date }}</td>
                        <td>{{ $delivery->time }}</td>
                        <td>{{ $delivery->delivery_address }}</td>
                        <td>{{ $delivery->phone_number }}</td>
                        <td>{{ @$delivery->assignDriver->user->name }}</td>
                        <td>{{ $delivery->deliveryStatusByCustomer() }}</td>
                    </tr>
                    @endforeach
            </tbody>
        </table>
    </div>

</div>
@endsection

@section('script')

<link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.css" />
<script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.js"></script>

<script>

    $(document).ready(function(){
        let table = new DataTable('#myTable', {
            responsive: true
        });
    });
</script>
@endsection
