@extends('layouts.admin-app')

@section ('title', 'Store')
@section('content')


<div class="top-titel">
    <div class="container ">
        <!-- <h1>Store</h1> -->
    </div>
</div>
<div class="container p-5 b-contnr">

    <div class="container mt-2">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-right">
                    <a class="btn btn-primary back-btn" href="{{ route('admin.store.index') }}"> Back</a>
                </div>
                <div class="pull-left mb-2 pt-3">
                    <h2>Create Store</h2>
                </div>

            </div>
        </div>
        @if(session('status'))
        <div class="alert alert-success mb-1 mt-1">
            {{ session('status') }}
        </div>
        @endif
        <form action="{{ route('admin.store.add') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Title</strong>
                        <input type="text" name="title" class="form-control" placeholder="Title">
                        @error('title')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Url</strong>
                        <input type="text" name="url" class="form-control" placeholder="Url">
                        @error('url')
                        <div class="alert alert-danger ">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Logo</strong>
                        <input type="file" name="file" class="form-control" placeholder="Choose image" id="image">
                        <img id="preview-image-before-upload" style="max-height: 70px;">


                        @error('file')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-primary edit-submit-btn ml-3">Submit</button>
            </div>
        </form>
    </div>


</div>
@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(e) {


        $('#image').change(function() {

            let reader = new FileReader();

            reader.onload = (e) => {

                $('#preview-image-before-upload').attr('src', e.target.result);
            }

            reader.readAsDataURL(this.files[0]);

        });

    });
</script>
