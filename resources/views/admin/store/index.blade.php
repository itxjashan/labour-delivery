@extends('layouts.admin-app')
@section ('title', 'Store')
@section('content')
<div class="container p-5 ">
    <div class="row">
        <div class="col-md-6 mb-4 ">
            <div class="pull-right">
               <h2> Stores </h2>
            </div>
        </div>
        <div class="col-md-6 mb-4 ">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('admin.store.create') }}" >Add New Store</a>
            </div>
        </div>

    </div>

    <div class="container mt-2 custom-table">

        <table class="table table-bordered pt-4 " id="myTable">
            <thead>
                <tr>
                    <th>Sr. No</th>
                    <th>Title</th>
                    <th>Url</th>
                    <th>Logo</th>
                    <th class="action">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($stores as $store)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td><b>{{ $store->title }}</b></td>
                        <td>{{ $store->url }}</td>
                        <td>
                            @if (!empty($store->getMedia('logo')))
                                <img src="{{ $store->getFirstMediaUrl('logo') }}" height="50px">
                            @endif
                        </td>
                        <td ><a class="btn btn-primary" href="{{ route('admin.store.edit',$store->id) }}">Edit</a>
                        <a class="btn btn-danger" href="{{ route('admin.store.destroy',$store->id) }}">Delete</a>

                        </td>
                    </tr>
                    @endforeach
            </tbody>
        </table>

    </div>

</div>
@endsection

@section('script')

<link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.css" />
<script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.js"></script>

<script>

    $(document).ready(function(){
        let table = new DataTable('#myTable', {
            responsive: true
        });
    });
</script>
@endsection
