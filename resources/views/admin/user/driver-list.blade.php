@extends('layouts.admin-app')
@section ('title', 'Drivers')
@section('content')
<div class="container p-5 ">

    <div class="row">
        <div class="col-md-6 mb-4 ">
            <div class="pull-right">
               <h2> Drivers </h2>
            </div>
        </div>

    </div>

    <div class="container mt-2 custom-table">

        <table class="table table-bordered pt-4 " id="myTable">
            <thead>
                <tr>
                    <th>Sr. No</th>
                    <th>Name</th>
                    <th>Email</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($drivers as $driver)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td><b>{{ $driver->name }}</b></td>
                        <td>{{ $driver->email }}</td>
                    </tr>
                    @endforeach
            </tbody>
        </table>
    </div>

</div>
@endsection

@section('script')

<link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.css" />
<script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.js"></script>

<script>

    $(document).ready(function(){
        let table = new DataTable('#myTable', {
            responsive: true
        });
    });
</script>
@endsection
