<script>
function initMap() {
  const addressInput = $('#address')[0];
  const autocomplete = new google.maps.places.Autocomplete(addressInput, {});

  autocomplete.addListener('place_changed', function() {
    const place = autocomplete.getPlace();
    const address = place.adr_address.replace(/(<([^>]+)>)/gi, "");
    const geocoder = new google.maps.Geocoder();

    const latlng = place.geometry.location;

    geocoder.geocode({ 'location': latlng }, function(results, status) {
      if (status === google.maps.GeocoderStatus.OK && results[0]) {
        const formattedAddress = results[0].formatted_address;
        // console.log(address, formattedAddress);
        @this.set('store_address', address);
      }
    });
  });
}

$(document).ready(function() {
  initMap();
});

</script>