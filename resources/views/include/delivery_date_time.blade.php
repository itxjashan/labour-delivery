<script type="text/javascript">
    var dt = localStorage.getItem("startDate");
    $("#txtenddate").datepicker({
        minDate: new Date(dt),
        onSelect: function(date) {
            $("#txtstartdate").datepicker('option', 'minDate', date);
            localStorage.setItem('endDate', date)
            @this.set('delivery_date', date);
        }
    });

    var startTime = localStorage.getItem("startTime");
    $('#timeEnd').timepicker({
        timeFormat: 'h:mm p',
        interval: 30,
        dynamic: false,
        dropdown: true,
        scrollbar: true,
        change: function(time) {
            var endtime = moment(time).format("hh:mm A");
            var endDate = localStorage.getItem("endDate");

            var beginningTime = moment(startTime, 'hh:mm A');
            var endTime = moment(endtime, 'hh:mm A');
            console.log('>>>>', beginningTime.isBefore(endTime));
            // if(dt == endDate) {
            //     if(!beginningTime.isBefore(endTime)) {
            //         alert("Delivery time cannot less than pickup time");
            //         $('#timeEnd').val('');
            //         return false;
            //     }
            // }

            @this.set('delivery_time', endtime);
        }
    });

    window.livewire.on('selectFire2', function(data, time) {
        $('.dte2').val(data);
        $('.tme2').val(time);
    });
</script>
