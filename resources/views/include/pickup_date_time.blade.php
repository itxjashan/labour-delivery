<script type="text/javascript">
    window.livewire.on('selectFire', function(data, time) {

        $("#txtstartdate").datepicker({
            minDate: 0,
            onSelect: function(date) {
                $("#txtenddate").datepicker('option', 'minDate', date);
                localStorage.setItem('startDate', date)
                @this.set('pickup_date', date);
            }
        });
        $('.dte').val(data);
        $('.tme').val(time);
        $("#txtenddate").datepicker({});

        $('#timepicker').timepicker({
            timeFormat: 'h:mm p',
            interval: 30,
            minTime: '5',
            dynamic: false,
            dropdown: true,
            scrollbar: true,
            change: function(time) {
                var strttime = moment(time).format("hh:mm A");
                localStorage.setItem('startTime', strttime)
                @this.set('pickup_time', strttime);
            }
        });
    });
</script>
