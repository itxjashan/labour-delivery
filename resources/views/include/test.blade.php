<script type="text/javascript">
    $(document).ready(function() {
      const txtStartDate = $("#txtstartdate");
      const txtEndDate = $("#txtenddate");
      const timepicker = $("#timepicker");
      const timeEnd = $("#timeEnd");

      txtStartDate.datepicker({
        minDate: 0,
        onSelect: function(date) {
          txtEndDate.datepicker('option', 'minDate', date);
          localStorage.setItem('startDate', date);
          @this.set('pickup_date', date);
        //   Livewire.emit('selectFire', date, timepicker.val());
        }
      });

      txtEndDate.datepicker({
        onSelect: function(date) {
          txtStartDate.datepicker('option', 'maxDate', date);
          localStorage.setItem('endDate', date);
          @this.set('delivery_date', date);
        //   Livewire.emit('selectFire2', date, timeEnd.val());
        }
      });

      timepicker.add(timeEnd).timepicker({
        timeFormat: 'h:mm p',
        interval: 30,
        dynamic: false,
        dropdown: true,
        scrollbar: true,
        change: function(time) {
          const timeValue = moment(time).format("hh:mm A");
          const startDate = localStorage.getItem("startDate");
          const endDate = localStorage.getItem("endDate");
          @this.set('pickup_time', startDate);
          @this.set('delivery_time', startDate);

          if ($(this).is(timepicker)) {
            localStorage.setItem('startTime', timeValue);
            Livewire.emit('selectFire', $('.dte').val(), timeValue);
          } else if ($(this).is(timeEnd)) {
            const startTime = localStorage.getItem("startTime");
            const beginningTime = moment(startTime, 'hh:mm A');
            const endingTime = moment(timeValue, 'hh:mm A');
            console.log('>>>>', beginningTime.isBefore(endingTime));
            Livewire.emit('selectFire2', $('.dte2').val(), timeValue);
          }
        }
      });

      Livewire.on('selectFire', function(data, time) {
        @this.set('pickup_date', $('.dte').val(data));
        @this.set('pickup_time', $('.tme').val(time));
      });

      Livewire.on('selectFire2', function(data, time) {
        $('.dte2').val(data);
        $('.tme2').val(time);
      });
    });
  </script>
