<div>
    <p class="delivery-text">Review Questions</p>
    <div class="row driver-form  ps-sm-5">
        @foreach ($questions as $question)
            <div class="col-md-6 py-3">
                <div class="review-checkbox-area">
                    <p>{{ $question->title }}</p>
                    <div class="form-check ">
                        <input class="form-check-input" wire:model="question.{{ $question->id }}" value="yes"
                            type="radio" checked>
                        <label class="form-check-label px-3">Yes</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" wire:model="question.{{ $question->id }}" value="no"
                            type="radio">
                        <label class="form-check-label px-3">
                            No
                        </label>
                    </div>

                </div>
                <span class="text-danger"> @error('question')
                        {{ $message }}
                    @enderror </span>
            </div>
        @endforeach

        <div class="col-md-12">
            <label class="form-label">Additional Comments</label>
            <textarea class="review-textarea" wire:model="review"></textarea>
            <span class="text-danger"> @error('review')
                    {{ $message }}
                @enderror </span>
        </div>
        <div class="col-md-6 pt-4 select-rating">
            <p>Select 1-5 star rating</p>
        </div>
        <div class="col-md-6 rating-section py-3">

            <div class="star-rating">
                <input type="radio" id="5-stars" wire:model="rating" value="5" />
                <label for="5-stars" class="star">&#9733;</label>
                <input type="radio" id="4-stars" wire:model="rating" value="4" />
                <label for="4-stars" class="star">&#9733;</label>
                <input type="radio" id="3-stars" wire:model="rating" value="3" />
                <label for="3-stars" class="star">&#9733;</label>
                <input type="radio" id="2-stars" wire:model="rating" value="2" />
                <label for="2-stars" class="star">&#9733;</label>
                <input type="radio" id="1-star" wire:model="rating" value="1" />
                <label for="1-star" class="star">&#9733;</label>
            </div>
            <span class="text-danger"> @error('rating')
                    {{ $message }}
                @enderror </span>
        </div>

        <div class="col-md-4 select-rating">
            <p>Leave a tip</p>
            <select class="form-select select-price" wire:model="tip" aria-label="Default select example">
                <option value="" selected>$</option>
                <option value="10">$10</option>
                <option value="20">$20</option>
                <option value="30">$30</option>
            </select>
            <span class="text-danger"> @error('tip') {{ $message }} @enderror </span>
        </div>

        <div class="col-md-8 crd-detail">
            <div class="card-section">
                <label class="card-payment">
                    <input class="" type="radio" wire:model="card_status" name="card_status" value="{{ $card->id }}">
                    <img src="{{ asset('img/011-visa.png') }}">
                    <p class="ml-1">XXXX XXXX XXXX {{ $card->card_number }}<br>{{ $card->card_name }}</p>
                </label>

                <label class="card-payment add-card">
                    <input class="" type="radio" wire:model="card_status" name="card_status" value="add_new_card">
                    <img src="{{ asset('img/new-card.png') }}">
                    <p class="ml-1">Add New Card</p>
                </label>
            </div>
            <span class="text-danger"> @error('card_status')
                {{ $message }}
            @enderror </span>
        </div>

        @if ($cart_error)
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">X</button>
                {!! $cart_error !!}
            </div>
        @endif
        @if ($checkCard)
            <div class="col-md-12">
                <label class="form-label">Name on Card</label>
                <input type="text" wire:model="card_name" class="form-control">
                <span class="text-danger"> @error('card_name')
                        {{ $message }}
                    @enderror </span>
            </div>
            <div class="col-md-12" x-data>
                <label class="form-label">Card Number</label>
                <input type="text" wire:model="card_number" x-mask="9999 9999 9999 9999" class="form-control">
                <span class="text-danger"> @error('card_number')
                        {{ $message }}
                    @enderror </span>

            </div>
            <div class="col-md-6" x-data>
                <label class="form-label">Exp Month</label>
                <input type="text" wire:model="exp_month" x-mask="99" class="form-control">
                <span class="text-danger"> @error('exp_month')
                        {{ $message }}
                    @enderror </span>

            </div>
            <div class="col-md-6" x-data>
                <label class="form-label">Exp Year</label>
                <input type="text" wire:model="exp_year" x-mask="9999" class="form-control">
                <span class="text-danger"> @error('exp_year')
                        {{ $message }}
                    @enderror </span>

            </div>
            <div class="col-md-6">
                <label class="form-label">CVV</label>
                <input type="text" wire:model="cvv" class="form-control">
                <span class="text-danger"> @error('cvv')
                        {{ $message }}
                    @enderror </span>
            </div>
        @endif

    </div>

    <div class="col-md-12">
        <div class="col-md-6 ">
            <button class="apply-btn" wire:click="store">
                <i wire:loading wire:target="store" class="fa fa-spin fa-spinner"></i> Complete Review
            </button>
        </div>
    </div>
</div>
</div>
