<div>
    <div class="row d-flex justify-content-between">
        <div class="col-md-6">
            <p class="delivery-text">Approved Deliveries</p>
        </div>
        <div class="col-md-5 p-sm-0 p-3 serch-btn" x-data="{ open: false }">
            <div class="searchBox" :class="{'active' : open, '' : !open}">
                <div class="search" @click="open = !open">
                    <img src="{{ asset('img/search.png') }}" alt="">
                </div>
                <div class="searchInput">
                    <input type="text" wire:model.debounce.1000ms="search" placeholder="Search Here">
                </div>

                <div class="close">
                    <ion-icon name="close" @click="open = !open">
                        <img src="{{ asset('img/close-search.png') }}" alt="">
                    </ion-icon>
                </div>
            </div>
        </div>
    </div>
    @if (count($deliveries) > 0)

        @foreach ($deliveries as $assignedDelivery)
            <div class="approved-deliveries mt-3">
                <div class="approved-parts px-xl-5 px-2 py-4">
                    <h5>{{ $assignedDelivery->delivery->store->title }}</h5>
                </div>
                <div class="approved-parts dlvry">
                    <p class="delivery-address">DELIVERY ADDRESS</p>
                    <p class="delivery-address-text">{{ $assignedDelivery->delivery->delivery_address }}</p>
                </div>
                <div class="approved-parts">
                    <p class="delivery-address">DELIVERY DATE & TIME</p>
                    <p class="delivery-date">{{ $assignedDelivery->delivery->date }}<br>
                        {{ $assignedDelivery->delivery->time }} Delivery
                    </p>
                </div>
                <div class="approved-parts stts">
                    <p class="delivery-address">Status</p>
                    <p class="delivery-address-text"> {{ $assignedDelivery->deliveryStatusByDriver() }} </p>
                </div>
                <div class="{{ $assignedDelivery->statusClass() }}">
                    <img src="{{ asset('img/' . $assignedDelivery->statusClass() . '.png') }}" alt="">

                    <a href="{{ route('driver.delivery-summary', encrypt($assignedDelivery->delivery->id)) }}">
                        <img src="{{ asset('img/view.png') }}" alt="">
                    </a>
                </div>
            </div>
        @endforeach
        <div>
            {{ $deliveries->links() }}
        </div>
    @else
        <p class="not-found-text"> Delivery Not Found</p>
    @endif

</div>
