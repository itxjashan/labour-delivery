<div>
    <div class="row d-flex justify-content-between">
        <div class="col-md-6">
            <p class="delivery-text">delivery Payouts</p>
        </div>
        <div class="col-md-5 p-sm-0 p-3 serch-btn" x-data="{ open: false }">
            <div class="searchBox" :class="{ 'active': open, '': !open }">
                <div class="search" @click="open = !open">
                    <img src="{{ asset('img/search.png') }}" alt="">
                </div>
                <div class="searchInput">
                    <input type="text" wire:model.debounce.1000ms="search" placeholder="Search Here">
                </div>

                <div class="close">
                    <ion-icon name="close" @click="open = !open">
                        <img src="{{ asset('img/close-search.png') }}" alt="">
                    </ion-icon>
                </div>
            </div>
        </div>
    </div>

    @if (count($payouts) > 0)
        @foreach ($payouts as $payout)
            <div class="approved-deliveries payout-main mt-3">
                <div class="approved-parts px-3 py-4">
                    <h5>{{ $payout->delivery->store->title }}</h5>
                </div>
                <div class="approved-parts dlvry">
                    <p class="delivery-address">DELIVERY ADDRESS</p>
                    <p class="delivery-address-text">{{ $payout->delivery->delivery_address }}</p>
                </div>
                <div class="approved-parts">
                    <p class="delivery-address">DELIVERY DATE</p>
                    <p class="delivery-date">{{ $payout->delivery->date }} </p>
                </div>
                <div class="approved-parts amt">
                    <p class="delivery-address">Amount</p>
                    <p class="delivery-date">${{ number_format($payout->total_amount, 2) }}</p>
                </div>
                <div class="approved-parts stts">
                    <p class="delivery-address">Status</p>
                    <p class="delivery-address-text"> Success </p>
                </div>
                <div class="correct-img n-tick">
                    <img src="{{ asset('img/correct-img.png') }}" wire:click="showModal({{ $payout->delivery_id }})"
                        alt="">
                    <a href="" alt="">
                    </a>
                </div>
            </div>
        @endforeach
        <div>
            {{ $payouts->links() }}
        </div>
    @else
        <p class="not-found-text"> Delivery Not Found</p>
    @endif

    <!-- Start Modal -->
    <div class="modal fade" id="checkAmount" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Delivery Amount</h5>
                    <button type="button" class="btn-close popup-btn" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    @if (!empty($amount))
                        <p>Delivery Amount = <b class="grn-price">${{ number_format($amount['delivery'], 2) }}</b></p>
                        @if ($amount['tip'] > 0)
                            <p>Tip Amount = <b class="grn-price">${{ number_format($amount['tip'], 2) }}</b></p>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
     <!-- End Modal -->

</div>

@push('scripts')
    <script>
        $(document).ready(function() {
            window.livewire.on('checkAmount', () => {
                $('#checkAmount').modal('show');
            })
        });
    </script>
@endpush
