<div class="charges-main">
    <p class="delivery-text">Complete Delivery</p>
    <p class="delivry">Delivery Charge = <span>$25 </span></p>
</div>
<div class="row driver-form  ps-sm-5">
    @if ($cart_error)
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">X</button>
            {!! $cart_error !!}
        </div>
    @endif
    <div class="col-md-12 complete-delivery-section my-3">

        <div class="complete-delivery-tabs active">
            <a href="#"><span><img src="{{ asset('img/new-card.png') }}"></span>Credit Card</a>
        </div>

        {{-- <div class="complete-delivery-tabs">
            <a href="#"><span><img src="{{ asset('img/paypal-svgrepo-com 1.png') }}"></span>Credit Card</a>
        </div>
        <div class="complete-delivery-tabs">
            <a href="#"><span><img src="{{ asset('img/venmo-svgrepo-com 1.png') }}"></span>Credit Card</a>
        </div>
        <div class="complete-delivery-tabs">
            <a href="#"><span><img src="{{ asset('img/square-cash-svgrepo-com 1.png') }}"></span>Credit Card</a>
        </div> --}}
    </div>
    @if ($card)
        <div class="row">
            <div class="col-md-12 crd-detail">
                {{-- <span>{{ @$card->card_name }}</span>
            <span>{{ @$card->card_number }}</span>
            <span>{{ @$card->exp_month }}</span>
            <span>{{ @$card->exp_year }}</span>
            <input type="radio" wire:model="card_status" name="card_status" value="{{ @$card->id }}">
            <input type="radio" wire:model="card_status" name="card_status" value="add_new"> --}}

                <div class="card-section">
                    <label class="card-payment">
                        <input class="" type="radio" wire:model="card_status" name="card_status" value="{{ $card->id }}">
                        <img src="{{ asset('img/011-visa.png') }}">
                        <p class="ml-1">XXXX XXXX XXXX {{ $card->card_number }}<br>{{ $card->card_name }}</p>
                    </label>

                    <label class="card-payment add-card">
                        <input class="" type="radio" wire:model="card_status" name="card_status" value="add_new_card">
                        <img src="{{ asset('img/new-card.png') }}">
                        <p class="ml-1">Add New Card</p>
                    </label>
                </div>
                <span class="text-danger"> @error('card_status')
                    {{ $message }}
                @enderror </span>
            </div>
        </div>
    @endif

    @if ($cardCheck)
        <div class="col-md-12">
            <label class="form-label">Name on Card</label>
            <input type="text" wire:model="card_name" class="form-control">
            <span class="text-danger"> @error('card_name')
                    {{ $message }}
                @enderror </span>
        </div>
        <div class="col-md-12" x-data>
            <label class="form-label">Card Number</label>
            <input type="text" wire:model="card_number" x-mask="9999 9999 9999 9999" class="form-control">
            <span class="text-danger"> @error('card_number')
                    {{ $message }}
                @enderror </span>
        </div>
        <div class="col-md-6" x-data>
            <label class="form-label">Exp Month</label>
            <input type="text" wire:model="exp_month" x-mask="99" class="form-control">
            <span class="text-danger"> @error('exp_month')
                    {{ $message }}
                @enderror </span>
        </div>
        <div class="col-md-6" x-data>
            <label class="form-label">Exp Year</label>
            <input type="text" wire:model="exp_year" x-mask="9999" class="form-control">
            <span class="text-danger"> @error('exp_year')
                    {{ $message }}
                @enderror </span>
        </div>
        <div class="col-md-6">
            <label class="form-label">CVV</label>
            <input type="text" wire:model="cvv" class="form-control">
            <span class="text-danger"> @error('cvv')
                    {{ $message }}
                @enderror </span>
        </div>
    @endif

</div>
