<!-- <div class="delivery-section"> -->
<div class="container delivery-see">
    <p class="delivery-text">Receipt Fields</p>
    <div class="row driver-form  ps-sm-5">
        <label class="form-label">Name of store </label>
        <div class="d-flex justify-content-between align-items-center store-name-main">
            <div class="col-md-10 pe-4 store-fld">
                <input type="text" class="form-control" wire:model="store_name" placeholder="Store" readonly>
                <span class="text-danger"> @error('store_name')
                    {{ $message }}
                    @enderror </span>
            </div>
            @if (!empty($store->getMedia('logo')))
            <div class="col-md-2 text-end store-pic">
                <img src="{{ $store->getFirstMediaUrl('logo') }}" alt="Name of store" class="home-depot-img">
            </div>
            @endif
        </div>

        <div class="col-md-12">
            <label class="form-label">Store Location Address
            </label>
            <input type="text" class="form-control" placeholder="Address" id="storeAddress" wire:model="store_address" wire:ignore>
            <span class="text-danger"> @error('store_address')
                {{ $message }}
                @enderror </span>
        </div>
        <div class="col-md-6">
            <label class="form-label">Pickup Date</label>
            <input type="text" class="form-control dte" id="txtstartdate" autocomplete="off" readonly="readonly" wire:ignore>
            <span class="text-danger"> @error('pickup_date')
                {{ $message }}
                @enderror </span>
        </div>
        <div class="col-md-6">
            <label class="form-label">Pickup Time</label>
            <input type="text" class="form-control tme" id="timepicker" autocomplete="off" readonly="readonly" wire:ignore>
            <span class="text-danger"> @error('pickup_time')
                {{ $message }}
                @enderror </span>
        </div>
    </div>
</div>

<script>
    function initMap() {
        const addressInput = $('#storeAddress')[0];
        const autocomplete = new google.maps.places.Autocomplete(addressInput, {});

        autocomplete.addListener('place_changed', function() {
            const place = autocomplete.getPlace();
            const address = place.adr_address.replace(/(<([^>]+)>)/gi, "");
            const geocoder = new google.maps.Geocoder();

            const latlng = place.geometry.location;

            geocoder.geocode({
                'location': latlng
            }, function(results, status) {
                if (status === google.maps.GeocoderStatus.OK && results[0]) {
                    const formattedAddress = results[0].formatted_address;
                    @this.set('store_address', address);
                }
            });
        });
    }

    $(document).ready(function() {
        initMap();
    });
</script>

@include('include.pickup_date_time')