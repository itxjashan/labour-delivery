<p class="delivery-text">Schedule Delivery</p>
<div class="row driver-form  ps-sm-5">
    @if ($customError)
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">X</button>
        {!! $customError !!}
    </div>
    @endif

    <div class="col-md-6">
        <label class="form-label">Delivery Date</label>
        <input type="text" class="form-control dte2" id="txtenddate" autocomplete="off" readonly="readonly" wire:ignore>
        <span class="text-danger"> @error('delivery_date'){{ $message }} @enderror </span>
    </div>
    <div class="col-md-6">
        <label class="form-label">Delivery Time</label>
        <input type="text" class="form-control tme2" id="timeEnd" autocomplete="off" readonly="readonly" wire:ignore>
        <span class="text-danger"> @error('delivery_time'){{ $message }} @enderror </span>
    </div>

    <div class="col-md-12 ">
        <label class="form-label">Delivery Location</label>
        <input type="text" class="form-control location-input" wire:model="delivery_address" id="deliveryAddress" wire:ignore>
        <span class="text-danger"> @error('delivery_address'){{ $message }} @enderror </span>
    </div>
    <div class="col-md-6" x-data>
        <label class="form-label">Contact Phone Number</label>
        <input type="text" class="form-control contact-input" x-mask="999 999 9999" wire:model="phone_number">
        <span class="text-danger"> @error('phone_number'){{ $message }} @enderror </span>
    </div>
    <div class="col-md-6">
        <label class="form-label">Customer Name</label>
        <input type="text" class="form-control contact-information" wire:model="customer_name">
        <span class="text-danger"> @error('customer_name'){{ $message }} @enderror </span>
    </div>
    <div class="col-md-12 select-rating">
        <label class="form-label">Pickup Needs</label>
        <select class="form-select pickup-need" aria-label="Default select example" wire:model="pickup_need">
            <option value="" selected>Select Pickup Need</option>
            <option value="truck_trailer">Truck or Trailer</option>
            <option value="heavy_equipment">Heavy Equipment</option>
        </select>
        <span class="text-danger"> @error('pickup_need'){{ $message }} @enderror </span>
    </div>
    <div class="col-md-12">
        <label class="form-label">Additional
            Notes</label>
        <textarea wire:model="additional_note"></textarea>
        <span class="text-danger"> @error('additional_note'){{ $message }} @enderror </span>
    </div>
</div>

<script>
    function initMap() {
        const addressInput = $('#deliveryAddress')[0];
        const autocomplete = new google.maps.places.Autocomplete(addressInput, {});

        autocomplete.addListener('place_changed', function() {
            const place = autocomplete.getPlace();
            const address = place.adr_address.replace(/(<([^>]+)>)/gi, "");
            const geocoder = new google.maps.Geocoder();

            const latlng = place.geometry.location;

            geocoder.geocode({
                'location': latlng
            }, function(results, status) {
                if (status === google.maps.GeocoderStatus.OK && results[0]) {
                    const formattedAddress = results[0].formatted_address;
                    @this.set('delivery_address', address);
                }
            });
        });
    }

    $(document).ready(function() {
        initMap();
    });
</script>

@include('include.delivery_date_time')
