<p class="delivery-text">Delivery Summary</p>
<div class="row driver-form   ps-sm-5">
    @if ($customError)
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">X</button>
            {!! $customError !!}
        </div>
    @endif
    <label  class="form-label">Name of store </label>
    <div class="d-flex justify-content-between align-items-center store-name-main">
        <div class="col-md-11 pe-4">
            <input type="text" class="form-control"  placeholder="The Home Depot" wire:model="store_name">
             <span class="text-danger"> @error('store_name'){{ $message }} @enderror </span>
        </div>

        <div class="col-md-1 text-end">
            @if (!empty($store->getMedia('logo')))
                <div class="col-md-1 text-end">
                    <img src="{{ $store->getFirstMediaUrl('logo') }}" alt="Name of store" class="home-depot-img">
                </div>
            @endif
        </div>
    </div>

    <div class="col-md-12">
        <label  class="form-label">Store Location Address </label>
        <input type="text" class="form-control" placeholder="Address" wire:model="store_address">
        <span class="text-danger"> @error('store_address'){{ $message }} @enderror </span>
    </div>
    <div class="col-md-6">
        <label  class="form-label">Pickup Date </label>
        <input type="text" class="form-control dte" id="txtstartdate" autocomplete="off" readonly="readonly" wire:ignore>
        <span class="text-danger"> @error('pickup_date'){{ $message }} @enderror </span>
    </div>
    <div class="col-md-6">
        <label  class="form-label">Pickup Time</label>
        <input type="text" class="form-control tme"  placeholder="time" id="timepicker" autocomplete="off" readonly="readonly" wire:ignore>
        <span class="text-danger"> @error('pickup_time'){{ $message }} @enderror </span>
    </div>
    <div class="col-md-12 select-rating">
        <label  class="form-label">Pickup Needs</label>
        <select class="form-select pickup-need" aria-label="Default select example" wire:model="pickup_need">
            <option value="" selected>Select Pickup Need</option>
            <option value="truck_trailer">Truck or Trailer</option>
            <option value="heavy_equipment">Heavy Equipment</option>
        </select>
        <span class="text-danger"> @error('pickup_need'){{ $message }} @enderror </span>
    </div>
    <div class="col-md-12">
        <label  class="form-label">Delivery Location Address</label>
        <input type="text" class="form-control" placeholder="Address" wire:model="delivery_address">
        <span class="text-danger"> @error('delivery_address'){{ $message }} @enderror </span>
    </div>
    <div class="col-md-6">
        <label  class="form-label">Delivery Date </label>
        <input type="text" class="form-control dte2"  placeholder="05/03/2023" id="txtenddate" autocomplete="off" readonly="readonly" wire:ignore>
        <span class="text-danger"> @error('delivery_date'){{ $message }} @enderror </span>
    </div>
    <div class="col-md-6">
        <label  class="form-label">Delivery Time</label>
        <input type="text" class="form-control tme2"  placeholder="time" id="timeEnd" autocomplete="off" readonly="readonly" wire:ignore>
        <span class="text-danger"> @error('delivery_time'){{ $message }} @enderror </span>
    </div>
    <div class="col-md-6">
        <label  class="form-label">Customer Name </label>
        <input type="text" class="form-control"  placeholder="Charles Perry" wire:model="customer_name">
        <span class="text-danger"> @error('customer_name'){{ $message }} @enderror </span>
    </div>
    <div class="col-md-6">
        <label  class="form-label">Customer Phone Number</label>
        <input type="text" class="form-control"  placeholder="555.555.5555" wire:model="phone_number">
        <span class="text-danger"> @error('phone_number'){{ $message }} @enderror </span>
    </div>
    <div class="col-md-12">
        <label  class="form-label">Additional Notes</label>
        <textarea wire:model="additional_note" placeholder="Additional Notes"></textarea>
        <span class="text-danger"> @error('additional_note'){{ $message }} @enderror </span>
    </div>

</div>

@include('include.delivery_date_time')

@include('include.pickup_date_time')
