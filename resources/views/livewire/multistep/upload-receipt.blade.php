<p class="delivery-text">Upload Your E-Receipt</p>
<div class="row driver-form ps-sm-5 mt-3">
    <div class="login-page">
        <div class="upload-image">
            @if ($e_reciept)
                <div wire:loading.delay wire:target="e_reciept">
                    <!-- Show loader while processing -->
                    <p>Loading...</p>
                </div>
                <div wire:loading.remove wire:target="e_reciept">
                    <!-- Display file name after selecting the file -->
                    <p>{{ $e_reciept->getClientOriginalName() }}</p>
                </div>
            @else
                <p class="file-name">Select an Image</p>
            @endif
            <input type="file" wire:model="e_reciept" accept="image/*,application/pdf" wire:change="validateFileType">

        </div>
        <span class="text-danger"> @error('e_reciept') {{ $message }} @enderror </span>
    </div>
</div
