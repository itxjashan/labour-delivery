<p class="delivery-text pb-4">Upload Confirmation Receipt</p>

<div class="upload-confirmation-img">

    @if ($e_reciept)
        @if ($e_reciept->getClientOriginalExtension() == 'pdf')
            <img src="/img/pdf-image.png">
        @else
            <img src="{{ $e_reciept->temporaryUrl() }}" alt="Preview">
        @endif
    @else
        <img src="/img/bill.png">
    @endif

</div>
{{--
<script>
    window.addEventListener('DOMContentLoaded', () => {
        Livewire.on('fileChosen', (inputFieldId, file) => {
            if (inputFieldId === 'image') {
                let reader = new FileReader();

                reader.onload = (e) => {
                    let imgElement = document.createElement('img');
                    imgElement.src = e.target.result;
                    imgElement.alt = 'Preview';

                    let previewContainer = document.querySelector('#preview-container');
                    previewContainer.innerHTML = '';
                    previewContainer.appendChild(imgElement);
                };

                reader.readAsDataURL(file);
            }
        });
    });
</script> --}}
